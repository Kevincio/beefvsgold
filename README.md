# README #

Quick overview of Laravel Test Project

### What is this repository for? ###

* Mucking about with Laravel 5.5 on PHP7

### Whats in it? ###

* Dumb little system that tracks the price of beef against the price of gold. 
* The APIRequestHttp model impliments an ApiRequest interface, the APIRquestHttp model is extended by the ApiRequestHttpBeef and ApiRequestHttpGold models. 
* These are called from the static FeedUpdater model via Laravels inbuild schedling. This call polls both of the feeds and updates the database accordingly.
* On the Front End a basic CanvasJS graph is used to show the data. Data is pulled from the database via the Eloquent model and then run though a CanvasJSHelper model that formats the data in a way that can be passed straight into the template.
* There are a few other bits and bobs in here, like the start of a rudimentey template system and comments page but they havn't been fleshed out yet.

### How do I get set up? ###

* Get Code
* Run Migrations + Seeds
* Add Laravel Scheduler to Crontab