@extends('template')

@section('title', 'Comment')

@section('content')
      <div class="starter-template">
        <h1>Add a Comment</h1>
        <p class="lead">Using the form below:</p>
      </div>

          <form method="post">
            <div class="row">
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name">
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="name">Email:</label>
                <input type="text" class="form-control" name="email">
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="name">Your Comment:</label>
                <textarea class="form-control" name="content"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Add Comment</button>
              </div>
            </div>
          </form>
@endsection