@extends('template')

@section('title', 'About')

@section('content')
      <div class="starter-template">
        <h1>About</h1>
        <p>The world runs on two things, beef and gold. This project is designed to keep everyone appraised of the situation.</p>
      </div>

@endsection