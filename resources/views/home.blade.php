@extends('template')

@section('title', 'Home')

@section('content')
      <div class="starter-template">
        <h1>Welcome</h1>
        <p class="lead">To Beef vs Gold</p>
      </div>

      <div>
        <div id="chartContainer" style="height: 370px; width: 100%;"></div>
      </div>

@endsection


@section('extrajs')
      <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
      <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
      <script type="text/javascript">
      $(document).ready(function(e){

      var options = {
      	exportEnabled: true,
      	animationEnabled: true,
      	title: {
      		text: "Gold Price vs Beef Price - Last 30 Days"
      	},
      	axisX: {
                valueFormatString: "DD-MMM",
                interval:1,
                intervalType: "day"
              },
      	data: [
      	{
      		type: "line",
      		name: "Gold Price (USD/Oz)",
      		legend: "Gold (1 USD/Oz)",
      		showInLegend: true,
      		dataPoints: [
                        {{$graphGold}}
                    ]
      	},
      	{
            type: "line",
      		name: "Beef Price (0.01 USD/Pound)",
      		legend: "Beef (0.01 USD/Oz)",
      		showInLegend: true,
            dataPoints: [
                        {{$graphBeef}}
                    ]
        }
      	]
      };
      $("#chartContainer").CanvasJSChart(options);
      });
      </script>
@endsection