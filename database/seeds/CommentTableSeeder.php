<?php

use Illuminate\Database\Seeder;
use App\Comment as Comment;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore 
        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex 
        ea commodo consequat.";

        $Comment = new Comment();
        $Comment->name = str_random(10);
        $Comment->email = str_random(12).'@example.com';
        $Comment->content = $content;
        $Comment->save();

        $Comment = new Comment();
        $Comment->name = str_random(10);
        $Comment->email = str_random(12).'@hotmail.com';
        $Comment->content = $content;
        $Comment->save();

        $Comment = new Comment();
        $Comment->name = str_random(10);
        $Comment->email = str_random(12).'@gmail.com';
        $Comment->content = $content;
        $Comment->save();
    }
}
