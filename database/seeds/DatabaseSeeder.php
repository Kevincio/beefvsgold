<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([CommodityTableSeeder::class, UserTableSeeder::class, CommentTableSeeder::class]);
    }
}
