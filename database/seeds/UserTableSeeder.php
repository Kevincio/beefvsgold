<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => str_random(8),
            'email' => str_random(12).'@example.com',
            'password' => bcrypt('secret'),
        ]);

        DB::table('users')->insert([
            'name' => str_random(8),
            'email' => str_random(12).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        DB::table('users')->insert([
            'name' => str_random(8),
            'email' => str_random(12).'@hotmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
