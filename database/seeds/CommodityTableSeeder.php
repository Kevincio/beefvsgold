<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommodityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 30; $i++)
        {
            $day = $i;
            if($day < 10)
            {
                $day = "0".$day;
            }
            else
            {
                $day = (string)$day;
            }

            DB::table('commodity')->insert([
                'type' => 1,
                'day' => '2017-10-'.$day,
                'value' => rand(200,400),
            ]);
        }

        for($i = 1; $i <= 30; $i++)
        {
            $day = $i;
            if($day < 10)
            {
                $day = "0".$day;
            }
            else
            {
                $day = (string)$day;
            }

            DB::table('commodity')->insert([
                'type' => 2,
                'day' => '2017-10-'.$day,
                'value' => rand(200,400),
            ]);
        }
    }
}
