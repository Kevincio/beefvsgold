<?php

namespace App\Interfaces;

/**
 * Interface ApiRequest
 *
 * Interface for making API calls
 */
interface ApiRequest
{
    /**
     * Make the API call, returning true/false for success/fail
     *
     * @return bool
     */
    public function call() : bool;

    /**
     * Get the results from the API call as an associative array
     *
     * @return array
     */
    public function getResultsAsArray() : array;
}