<?php

namespace App\Models;

/**
 * Helper class for use with the CanvasJS Javascript library
 *
 * Class CanvasJSHelper
 * @package App\Models
 */
abstract class CanvasJSHelper
{
    /**
     * Takes a associative array of results with a day and value and converts them into a string format that can
     * be injected into a blade template with dataPoints of graph data
     *
     * @param array $results
     * @return string
     */
    static function parse(array $results) : string
    {
        $returnString = '';
        $numResults = count($results);

        if($numResults > 0)
        {
            $elements = 1;
            foreach($results as $res)
            {
                // We're basically building data into a javascript array/object notation we can use inline
                $dateBits = explode("-", $res['day']);
                
                $returnString .= "{ x: new Date(".$dateBits[0].", ".$dateBits[1].", ".$dateBits[2]."), y: ".$res['value']." }";

                // Don't use a comma on the last line
                if($numResults == $elements)
                {
                    $returnString .= "\n";
                }
                else{
                    $returnString .= ",\n";
                }

                $elements++;
            }
        }

        return $returnString;
    }
}