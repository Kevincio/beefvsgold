<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Class FeedUpdater, processes updates of API feeds
 * @package App\Models
 */
abstract class FeedUpdater
{
    /**
     * Run function
     */
    static function run()
    {
        // Set up the details for our two feeds
        $goldType = 1;
        $beefType = 2;

        $goldParams = [
            'api_key' => 'sngj4QKRFcvCRX9LaC_Q',
            'limit'   => '30'
            ];

        $beefParams = [
            'api_key' => 'sngj4QKRFcvCRX9LaC_Q',
            'limit'   => '30'
        ];

        // Call the first feed
        $ApiRequestHttpGold = new ApiRequestHttpGold();
        $ApiRequestHttpGold->setParameters($goldParams);

        // If we have results, update the database
        if($ApiRequestHttpGold->call() === true)
        {
            $goldResults = $ApiRequestHttpGold->getResultsAsArray();

            foreach($goldResults as $res)
            {
                $sql = "INSERT INTO commodity 
                        (type, day, value) 
                          VALUES 
                        (:type, :day, :value) ON DUPLICATE KEY UPDATE value=:value";
                $params = [
                    'type' => $goldType,
                    'day'  => $res['day'],
                    'value'=> $res['value']
                ];
                DB::insert($sql, $params);
            }
        }

        // Call our second feed
        $ApiRequestHttpBeef = new ApiRequestHttpBeef();
        $ApiRequestHttpBeef->setParameters($beefParams);

        // Again, if we have results update the database
        if($ApiRequestHttpGold->call() === true)
        {
            $beefResults = $ApiRequestHttpBeef->getResultsAsArray();

            foreach($beefResults as $res)
            {
                $sql = "INSERT INTO commodity 
                        (type, day, value) 
                          VALUES 
                        (:type, :day, :value) ON DUPLICATE KEY UPDATE value=:value";
                $params = [
                    'type' => $beefType,
                    'day'  => $res['day'],
                    'value'=> $res['value']
                ];
                DB::insert($sql, $params);
            }
        }
    }
}