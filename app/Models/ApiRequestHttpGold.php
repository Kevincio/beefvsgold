<?php

namespace App\Models;

use App\Models\ApiRequestHttp as ApiRequestHttp;

/**
 * Class ApiRequestHttpGold
 * @package App\Models
 */
class ApiRequestHttpGold extends ApiRequestHttp
{
    /**
     * ApiRequestHttpGold constructor.
     */
    public function __construct()
    {
        $this->requestUrl = 'https://www.quandl.com/api/v3/datasets/LBMA/GOLD/data.json';
    }

    /**
     * @return bool
     */
    public function call() : bool
    {
        // Build the full request string from the URL and parameters
        $fullRequest = $this->requestUrl.$this->buildParams();

        // Use file get contents to easily grab the result
        $result = file_get_contents($fullRequest);

        if($result === false)
        {
            // If we've had a problem, empty the result array and return false
            $this->resultsArray = [];
            return false;
        }
        else
        {
            // If everything went well, json decode the results and put them in our result array
            $returnArray = json_decode($result, true);

            foreach($returnArray['data'] as $key => $val)
            {
                $this->resultsArray[] = ['day' => $val[0], 'value' => $val[1]];
            }

            return true;
        }
    }
}

