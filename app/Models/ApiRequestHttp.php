<?php

namespace App\Models;

use App\Interfaces\ApiRequest as ApiRequest;

abstract class ApiRequestHttp implements ApiRequest
{
    /**
     * Array containing our results
     *
     * @var array
     */
    protected $resultsArray;

    /**
     * Bool result of our last API call
     *
     * @var bool
     */
    protected $result;

    /**
     * The URL to hit with our request
     *
     * @var string
     */
    protected $requestUrl;

    /**
     * The parameters to send with our request
     *
     * @var
     */
    protected $parameters;

    /**
     * Make the API call, returning true/false for success/fail
     *
     * @return bool
     */
    abstract public function call() : bool;

    /**
     * Sets the parameters to send along with the request
     *
     * @param array $params
     */
    public function setParameters(array $params)
    {
        $this->parameters = $params;
    }

    /**
     * Get the results from the API call as an associative array
     *
     * @return array
     */
    public function getResultsAsArray() : array
    {
        return $this->resultsArray;
    }

    /**
     * Take our array of params from $this->parameters and return as a URL GET request string
     *
     * @return string
     */
    protected function buildParams()
    {
        // String to build
        $params = '';

        // Check we actually have some parameters to parse
        if(count($this->parameters) > 0)
        {
            // Keep track of the element number for positional purposes
            $elementNumber = 0;
            foreach($this->parameters as $key => $value)
            {
                // First element needs a ? prefix, rest need an &
                if($elementNumber == 0)
                {
                    $prefix = "?";
                }
                else
                {
                    $prefix = "&";
                }

                $params .= $prefix.$key."=".$value;

                $elementNumber++;
            }
        }

        return urlencode($params);
    }
}