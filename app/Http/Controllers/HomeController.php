<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Commodity as Commodity;
use App\Models\CanvasJSHelper as CanvasJSHelper;

class HomeController extends Controller
{

    /**
     * Loads the homepage view
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get our gold values
        $commoditiesGold = Commodity::where('type', '=', 1)->select('day', 'value')->orderBy('day', 'desc')->take(30)->get()->toArray();
        // And our Beef values
        $commoditiesBeef = Commodity::where('type', '=', 2)->select('day', 'value')->orderBy('day', 'desc')->take(30)->get()->toArray();

        // Format for use in the template
        $goldContent = CanvasJSHelper::parse($commoditiesGold);
        $beefContent = CanvasJSHelper::parse($commoditiesBeef);

        return view('home',
            [
                'route' => Route::current()->uri,
                'graphGold' => $goldContent,
                'graphBeef' => $beefContent
            ]);
    }
}