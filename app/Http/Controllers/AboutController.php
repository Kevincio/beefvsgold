<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;

class AboutController extends Controller
{

    /**
     * Loads the about view
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('about', ['route' => Route::current()->uri]);
    }
}